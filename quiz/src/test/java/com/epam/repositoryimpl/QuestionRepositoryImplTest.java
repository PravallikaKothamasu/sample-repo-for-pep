package com.epam.repositoryimpl;


import com.epam.datamodel.Difficulty;
import com.epam.datamodel.Option;
import com.epam.datamodel.Question;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith(MockitoExtension.class)
class QuestionRepositoryImplTest {

    private QuestionRepositoryImpl questionRepositoryImpl;
//    private List<Question> questions;

    private Question question;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    EntityManager entityManager;

    @BeforeEach
    public void setup() {
        initMocks(this);
        questionRepositoryImpl = new QuestionRepositoryImpl();
//        List<List<Option>> optionsList = new ArrayList<>();
//
//        optionsList.add(Arrays.asList(
//                new Option("OptionDescription1", false),
//                new Option("OptionDescription2", false),
//                new Option("OptionDescription3", false),
//                new Option("OptionDescription4", true)
//        ));
//
//        optionsList.add(Arrays.asList(
//                new Option("OptionDescription1", false),
//                new Option("OptionDescription2", false),
//                new Option("OptionDescription3", true),
//                new Option("OptionDescription4", false)
//        ));
//
//        questions = Arrays.asList(
//                new Question( "QDes1", 3, Difficulty.MEDIUM),
//                new Question("QDes2", 2, Difficulty.EASY)
//        );
//
//        questions.forEach(question -> questionRepositoryImpl.insertQuestion(question));
        List<Option> options = new ArrayList<>();
        options.add(new Option("OptionDescription1", false));
        options.add(new Option("OptionDescription2", false));
        options.add(new Option("OptionDescription3", false));
        options.add(new Option("OptionDescription4", true));
        question = new Question( "QDes4", 2, Difficulty.MEDIUM);
        question.setQuestionId(1L);
        question.setOptions(options);
        System.out.println(question.toString());
    }

    @Test
    void testGetAllQuestion() {
        when(entityManager.createQuery(argThat(queryString -> queryString.equals("SELECT q FROM Question q")),eq(Question.class)).getResultList())
                .thenReturn(Collections.singletonList(question));
        List<Question> questionList = questionRepositoryImpl.getAllQuestion();
       //assertEquals(1, questionList.size());
        System.out.println(questionList);
        assertEquals(question.toString(), questionList.get(0).toString());
    }

    @Test
    public void testInsertQuestionWhenQuestionIsNotNull() {
        // when
        boolean result = questionRepositoryImpl.insertQuestion(question);
        // then
        assertTrue(result);
    }

    @Test
    public void testInsertQuestionWhenQuestionIsNull() {
        // when
        boolean result = questionRepositoryImpl.insertQuestion(null);
        //then
        assertFalse(result);
    }

    @Test
    public void testDeleteQuestionWhenQuestionIdNotPresent() {
        // when
        boolean result = questionRepositoryImpl.deleteQuestion(8L);
        // then
        assertFalse(result);
    }

    @Test
    public void testDeleteQuestionWithQuestionIdIsPresent() {
        // when
        boolean result = questionRepositoryImpl.deleteQuestion(1L);
        // then
        assertTrue(result);
    }

    @Test
    public void testUpdateQuestionWithQuestionIdPresentAndQuestionIsNotNull() {
        // when
        boolean result = questionRepositoryImpl.updateQuestion(question, 1L);
        // then
        assertTrue(result);
    }

    @Test
    public void testUpdateQuestionWithQuestionIdNotPresentAndQuestionIsNotNull() {
        // when
        boolean result = questionRepositoryImpl.updateQuestion(question, 6L);
        // then
        assertFalse(result);
    }

    @Test
    public void testUpdateQuestionWithQuestionIdNotPresentAndQuestionIsNull() {
        // when
        boolean result = questionRepositoryImpl.updateQuestion(null, 6L);
        // then
        assertFalse(result);
    }

    @Test
    public void testUpdateQuestionWithQuestionIdIsPresentAndQuestionIsNull() {
        // when
        boolean result = questionRepositoryImpl.updateQuestion(null, 2L);
        // then
        assertFalse(result);
    }

    @Test
    public void testGetQuestionWhenQuestionIdIsPresent() {
        // when
        Optional<Question> resultOptionalQuestion = questionRepositoryImpl.getQuestion(2L);

        // then
//        assertTrue(resultOptionalQuestion.isPresent());
//        assertEquals(questions.get(1).getQuestionDescription(), resultOptionalQuestion.get().getQuestionDescription());
//        assertEquals(questions.get(1).getQuestionScore(), resultOptionalQuestion.get().getQuestionScore());
//        assertEquals(questions.get(1).getDifficulty().name(), resultOptionalQuestion.get().getDifficulty().name());
//        assertEquals(questions.get(1).getOptions().size(), resultOptionalQuestion.get().getOptions().size());
    }

    @Test
    public void testGetQuestionWhenQuestionIdIsNotPresent() {
        // When
        Optional<Question> resultOptionalQuestion = questionRepositoryImpl.getQuestion(5L);

        // then
        assertFalse(resultOptionalQuestion.isPresent());
        assertEquals(Optional.empty(), resultOptionalQuestion);
    }

}