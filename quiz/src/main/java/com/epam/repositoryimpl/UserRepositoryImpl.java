package com.epam.repositoryimpl;

import com.epam.datamodel.Users;
import com.epam.repository.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import java.util.Objects;

@Component
public class UserRepositoryImpl implements UserRepository {

    private static final EntityManager entityManager = Persistence
            .createEntityManagerFactory("QuizJPA")
            .createEntityManager();
    private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);

    @Override
    public boolean signUp(Users user) {
        boolean success = false;
        if (Objects.nonNull(user)) {
            entityManager.getTransaction().begin();
            if (findUserByEmailId(user.getEmailId()) == null) {
                user.setPassword(encoder.encode(user.getPassword()));
                entityManager.persist(user);
            }
            entityManager.getTransaction().commit();
            success = true;
        }
        return success;
    }

    @Override
    public boolean login(String email, String password) {
        boolean success = false;
        Users fetchUser = findUserByEmailId(email);
        if (Objects.nonNull(fetchUser)) {
            success = validateEmailPassword(email, password, fetchUser);
        }
        return success;
    }

    private Users findUserByEmailId(String emailId) {
        Users getUser = null;
        try {
            getUser= entityManager
                    .createQuery("SELECT u FROM User u WHERE u.emailId = :emailId", Users.class)
                    .setParameter("emailId", emailId)
                    .getSingleResult();
        }
        catch (NoResultException exception) {
            LogManager.getLogger(UserRepositoryImpl.class).error(exception.getMessage());
        }
        return getUser;
    }

    private boolean validateEmailPassword(String email, String password, Users user) {
        boolean isValid = false;
        if (!email.isEmpty() && !password.isEmpty() && encoder.matches(password, user.getPassword()) && email.equals(user.getEmailId())) {
            isValid = true;
        }
        return isValid;
    }
}
