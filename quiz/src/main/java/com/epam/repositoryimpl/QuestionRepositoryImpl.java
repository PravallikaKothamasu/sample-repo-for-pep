package com.epam.repositoryimpl;

import com.epam.datamodel.Question;
import com.epam.repository.QuestionRepository;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class QuestionRepositoryImpl implements QuestionRepository {

    private static final EntityManager entityManager = Persistence
            .createEntityManagerFactory("QuizJPA")
            .createEntityManager();

    @Override
    public List<Question> getAllQuestion() {
         Query query = entityManager.createQuery("SELECT q FROM Question q", Question.class);
        return query.getResultList();
    }

    @Override
    public boolean insertQuestion(Question question) {
        boolean success = false;
        if (Objects.nonNull(question)) {
            entityManager.getTransaction().begin();
            entityManager.persist(question);
            entityManager.getTransaction().commit();
            success = true;
        }
        return success;
    }

    @Override
    public boolean deleteQuestion(Long questionId) {
        boolean success = false;
        entityManager.getTransaction().begin();
        Question question = entityManager.find(Question.class, questionId);
        if (question != null) {
            entityManager.remove(question);
            success = true;
        }
        entityManager.getTransaction().commit();
        return success;
    }

    @Override
    public boolean updateQuestion(Question question, Long questionId) {
        boolean success = false;
        entityManager.getTransaction().begin();
        Question fetchQuestion = entityManager.find(Question.class, questionId);
        if (Objects.nonNull(question) && Objects.nonNull(fetchQuestion)) {
            fetchQuestion.setQuestionDescription(question.getQuestionDescription());
            fetchQuestion.setQuestionScore(question.getQuestionScore());
            fetchQuestion.setDifficulty(question.getDifficulty());
            fetchQuestion.setOptions(question.getOptions());
            entityManager.persist(fetchQuestion);
            success = true;
        }
        entityManager.getTransaction().commit();
        return success;
    }

    @Override
    public Optional<Question> getQuestion(Long questionId) {
        Question question = entityManager.find(Question.class, questionId);
        return question != null ? Optional.of(question) : Optional.empty();
    }
}
