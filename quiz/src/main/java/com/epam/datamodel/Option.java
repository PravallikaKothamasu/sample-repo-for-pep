package com.epam.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "choice")
public class Option {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long optionId;

    @Column(nullable = false, columnDefinition = "LONGTEXT")
    private String optionDescription;

    @Column(nullable = false)
    private boolean isCorrect;

    @ManyToOne(targetEntity = Question.class)
    @JoinColumn(name = "questionId", referencedColumnName = "questionId", nullable = false)
    private Question question;

    public Option() {

    }

    public Option(String optionDescription, boolean isCorrect) {
        this.optionDescription = optionDescription;
        this.isCorrect = isCorrect;
    }

    public String getOptionDescription() {
        return optionDescription;
    }

    public void setOptionDescription(String optionDescription) {
        this.optionDescription = optionDescription;
    }

    public boolean getIsCorrect() {
        return isCorrect;
    }

    public void setIsCorrect(boolean correct) {
        isCorrect = correct;
    }

    public Long getOptionId() {
        return optionId;
    }

    public void setOptionId(Long optionId) {
        this.optionId = optionId;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "Option{" +
                "optionId=" + optionId +
                ", optionDescription='" + optionDescription + '\'' +
                ", isCorrect=" + isCorrect +
                '}';
    }
}
