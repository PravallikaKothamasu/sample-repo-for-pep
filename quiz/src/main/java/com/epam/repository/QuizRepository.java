package com.epam.repository;

import com.epam.datamodel.Quiz;

import java.util.List;
import java.util.Optional;

public interface QuizRepository {
    boolean createQuiz(Quiz quiz);
    boolean deleteByQuizId(Long quizId);
    List<Quiz> getAllQuiz();
    Optional<Quiz> getQuizById(Long quizId);
}
