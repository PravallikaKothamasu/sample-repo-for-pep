package com.epam.repository;

import com.epam.datamodel.Question;

import java.util.List;
import java.util.Optional;

public interface QuestionRepository {
    List<Question> getAllQuestion();
    boolean insertQuestion(Question question);
    boolean deleteQuestion(Long questionId);
    boolean updateQuestion(Question question, Long questionId);
    Optional<Question> getQuestion(Long questionId);
}
