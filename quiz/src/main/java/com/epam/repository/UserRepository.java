package com.epam.repository;


import com.epam.datamodel.Users;

public interface UserRepository {
    boolean signUp(Users user);
    boolean login(String email, String password);

}
